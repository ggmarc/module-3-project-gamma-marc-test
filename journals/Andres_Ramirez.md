Day 1 - 6/26

We worked on setting up Trello to be used in lieu of Gitlab issue board. Tonight, we will finish the authentication video. No blockers.

Day 2 - 6/27

Last night I spent about 4 hours studying authentication in SQL and encountered some errors I have yet to resolve. Today our team will attack authentication as a unit. No blockers.

Day 3 - 6/28

Yesterday, our team finished backend authentication except for the duplicate error class. Today, we plan on tackling that as well as as many API endpoints as we can. No blockers.

Day 4 - 6/29

Yesterday, our team successfully established a foreign key relationship between the account and dog tables. We also finished the create dog API endpoint. Today, we plan on tackling list dog, delete dog, and edit dog. No blockers.

Day 5 - 6/30

Yesterday, our team finished all backend API endpoints aside from update, which we started but had a bug on. Today, we will be debugging the update API endpoint, reviewing our codebase, and starting the frontend. No blockers.
